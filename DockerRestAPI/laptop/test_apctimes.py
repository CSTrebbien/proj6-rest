"""
Nose tests for apctimes

"""
from acp_times import open_time, close_time
import nose 
import logging
import arrow
logging.basicConfig(format='%(levelname)s:%(message)s',
                    level=logging.WARNING)
log = logging.getLogger(__name__)

time = arrow.get("2017-01-01T00:00:00+00:00").isoformat()

def test_zero_checkpoint():
    """
    test to make sure it shifts by 1 hour
    """
    close = arrow.get(close_time(0, 200, time))
    assert(close.format("YYYY-MM-DD HH:mm") == "2017-01-01 01:00")
    test = arrow.get(open_time(0, 200, time))
    assert(test.format("YYYY-MM-DD HH:mm") == "2017-01-01 00:00")


def test_standard():
    """
    test a standard input
    """
    close = arrow.get(close_time(60, 200, time))
    assert(close.format("YYYY-MM-DD HH:mm") == "2017-01-01 04:00")
    test = arrow.get(open_time(60, 200, time))
    assert(test.format("YYYY-MM-DD HH:mm") == "2017-01-01 01:46")
    close = arrow.get(close_time(120, 200, time))
    assert(close.format("YYYY-MM-DD HH:mm") == "2017-01-01 08:00")
    test = arrow.get(open_time(120, 200, time))
    assert(test.format("YYYY-MM-DD HH:mm") == "2017-01-01 03:32")
    close = arrow.get(close_time(175, 200, time))
    assert(close.format("YYYY-MM-DD HH:mm") == "2017-01-01 11:40")
    test = arrow.get(open_time(175, 200, time))
    assert(test.format("YYYY-MM-DD HH:mm") == "2017-01-01 05:09")
    close = arrow.get(close_time(205, 200, time))
    assert(close.format("YYYY-MM-DD HH:mm") == "2017-01-01 13:30")
    test = arrow.get(open_time(205, 200, time))
    assert(test.format("YYYY-MM-DD HH:mm") ==  "2017-01-01 05:53")

def test_FRENCH_PEOPLE():
    close = arrow.get(close_time(20, 200, time))
    assert(close.format("YYYY-MM-DD HH:mm") == "2017-01-01 02:00")
    test = arrow.get(open_time(20, 200, time))
    assert(test.format("YYYY-MM-DD HH:mm") == "2017-01-01 00:35")
   
def test_bigboy():
    """
    this test is big and beefy. This test checks to make sure
    that as the controls get father along that the algorithm
    is working past each brevit speed time change
    """
    
    #CLOSE TIMES
    close = arrow.get(close_time(100, 1000, time))
    assert(close.format("YYYY-MM-DD HH:mm") == "2017-01-01 06:40")
    close = arrow.get(close_time(200, 1000, time))
    assert(close.format("YYYY-MM-DD HH:mm") == "2017-01-01 13:20")
    close = arrow.get(close_time(300, 1000, time))
    assert(close.format("YYYY-MM-DD HH:mm") == "2017-01-01 20:00")
    close = arrow.get(close_time(400, 1000, time))
    assert(close.format("YYYY-MM-DD HH:mm") == "2017-01-02 02:40")
    close = arrow.get(close_time(500, 1000, time))
    assert(close.format("YYYY-MM-DD HH:mm") == "2017-01-02 09:20")
    close = arrow.get(close_time(600, 1000, time))
    assert(close.format("YYYY-MM-DD HH:mm") == "2017-01-02 16:00")
    close = arrow.get(close_time(700, 1000, time))
    assert(close.format("YYYY-MM-DD HH:mm") == "2017-01-03 00:45")
    close = arrow.get(close_time(800, 1000, time))
    assert(close.format("YYYY-MM-DD HH:mm") == "2017-01-03 09:30")
    close = arrow.get(close_time(900, 1000, time))
    assert(close.format("YYYY-MM-DD HH:mm") == "2017-01-03 18:15")
    close = arrow.get(close_time(1000, 1000, time))
    assert(close.format("YYYY-MM-DD HH:mm") == "2017-01-04 03:00")

    #OPEN TIMES
    close = arrow.get(open_time(100, 1000, time))
    assert(close.format("YYYY-MM-DD HH:mm") == "2017-01-01 02:56")
    close = arrow.get(open_time(200, 1000, time))
    assert(close.format("YYYY-MM-DD HH:mm") == "2017-01-01 05:53")
    close = arrow.get(open_time(300, 1000, time))
    assert(close.format("YYYY-MM-DD HH:mm") == "2017-01-01 09:00")
    close = arrow.get(open_time(400, 1000, time))
    assert(close.format("YYYY-MM-DD HH:mm") == "2017-01-01 12:08")
    close = arrow.get(open_time(500, 1000, time))
    assert(close.format("YYYY-MM-DD HH:mm") == "2017-01-01 15:28")
    close = arrow.get(open_time(600, 1000, time))
    assert(close.format("YYYY-MM-DD HH:mm") == "2017-01-01 18:48")
    close = arrow.get(open_time(700, 1000, time))
    assert(close.format("YYYY-MM-DD HH:mm") == "2017-01-01 22:22")
    close = arrow.get(open_time(800, 1000, time))
    assert(close.format("YYYY-MM-DD HH:mm") == "2017-01-02 01:57")
    close = arrow.get(open_time(900, 1000, time))
    assert(close.format("YYYY-MM-DD HH:mm") == "2017-01-02 05:31")
    close = arrow.get(open_time(1000, 1000, time))
    assert(close.format("YYYY-MM-DD HH:mm") == "2017-01-02 09:05")

def test_french():
    close = arrow.get(close_time(200, 200, time))
    assert(close.format("YYYY-MM-DD HH:mm") == "2017-01-01 13:30")
    close = arrow.get(close_time(300, 300, time))
    assert(close.format("YYYY-MM-DD HH:mm") == "2017-01-01 20:00")
    close = arrow.get(close_time(400, 400, time))
    assert(close.format("YYYY-MM-DD HH:mm") == "2017-01-02 03:00")
    close = arrow.get(close_time(600, 600, time))
    assert(close.format("YYYY-MM-DD HH:mm") == "2017-01-02 16:00")
    close = arrow.get(close_time(1000, 1000, time))
    assert(close.format("YYYY-MM-DD HH:mm") == "2017-01-04 03:00")









